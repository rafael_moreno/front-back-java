package br.com.neppo.personmanager.service;

import br.com.neppo.personmanager.entity.PersonEntity;
import br.com.neppo.personmanager.model.SexType;
import br.com.neppo.personmanager.repository.PersonRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class PersonServiceIT {

    private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private static boolean initiate = false;
    private static PersonEntity newPerson;
    private static PersonEntity person1;
    private static Page<PersonEntity> personsPage0;
    private static Page<PersonEntity> personsPage1;
    private static Page<PersonEntity> personsPageOrderSex;
    private static Page<PersonEntity> personsPageFilterName;
    //private static List<PersonEntity> persons;

    @Mock
    private PersonRepository personRepository;

    @InjectMocks
    private PersonService personService;


    @Before
    public void initPersons() throws ParseException {
        if (!initiate) {
            newPerson = new PersonEntity(null, "new person", "0192300003", dateFormat.parse("01/01/2010"), SexType.MALE, "Rua new, 1");
            person1 = new PersonEntity(1L, "person1", "0192391023", dateFormat.parse("01/01/2010"), SexType.MALE, "Rua um, 1");

            Sort sort = new Sort(Sort.Direction.ASC, "name");
            Sort sortSex = new Sort(Sort.Direction.DESC, "sex");
            Pageable pageable0 = PageRequest.of(0, 2, sort);
            Pageable pageable1 = PageRequest.of(1, 2, sort);
            Pageable pageableSex = PageRequest.of(0, 2, sortSex);
            PersonEntity p1 = new PersonEntity(1L, "zeze", "0192000023", dateFormat.parse("01/01/2010"), SexType.MALE, "Rua uno, 1");
            PersonEntity p2 = new PersonEntity(2L, "bruna", "0192222223", dateFormat.parse("01/01/2010"), SexType.FEMALE, "Rua dois, 22");
            PersonEntity p3 = new PersonEntity(3L, "francisco", "0199991023", dateFormat.parse("01/01/2010"), SexType.MALE, "Rua tres, 333");
            PersonEntity p4 = new PersonEntity(4L, "xunaymes", "1192391023", dateFormat.parse("01/01/2010"), SexType.FEMALE, "Rua quatro, 44");
            personsPage0 = new PageImpl<>(Arrays.asList(p2, p3), pageable0, 4);
            personsPage1 = new PageImpl<>(Arrays.asList(p4, p1), pageable1, 4);
            personsPageOrderSex = new PageImpl<>(Arrays.asList(p1, p3), pageableSex, 4);
            personsPageFilterName = new PageImpl<>(Arrays.asList(p2, p4), pageable0, 2);

            initiate = true;
        }
    }

    @Test
    public void checkCreatePerson() {
        when(personRepository.findByDocument("0192300003")).thenReturn(null);
        when(personRepository.save(newPerson)).thenReturn(person1);

        String error = "";
        try {
            personService.saveNewPerson(newPerson);
        } catch (Exception e) {
            error = e.getMessage();
        }
        Assert.assertEquals("", error);
    }

    @Test
    public void checkCreatePerson_alreadExists() {
        when(personRepository.findByDocument("0192300003")).thenReturn(person1);

        String error = "";
        try {
            personService.saveNewPerson(newPerson);
        } catch (Exception e) {
            error = e.getMessage();
        }
        Assert.assertEquals(person1.toString(), error);
    }

    @Test
    public void checkUpdatePerson() {
        when(personRepository.findById(1L)).thenReturn(Optional.of(person1));
        PersonEntity changedPerson = new PersonEntity(1L, newPerson.getName(), newPerson.getDocument(),
                newPerson.getBirthdate(), newPerson.getSex(), newPerson.getAddress());
        when(personRepository.save(any(PersonEntity.class))).thenReturn(changedPerson);

        String error = "";
        try {
            personService.updatePerson(1L, newPerson);
        } catch (Exception e) {
            error = e.getMessage();
        }
        Assert.assertEquals("", error);
    }

    @Test
    public void checkUpdatePerson_NotFound() {
        when(personRepository.findById(1L)).thenReturn(Optional.empty());

        String error = "";
        try {
            personService.updatePerson(1L, newPerson);
        } catch (Exception e) {
            error = e.getClass().getSimpleName();
        }
        Assert.assertEquals("NotFoundException", error);
    }


    @Test
    public void checkDeletePerson() {
        when(personRepository.findById(1L)).thenReturn(Optional.of(person1));
        doNothing().when(personRepository).delete(person1);

        String error = "";
        try {
            personService.deletePerson(1L);
        } catch (Exception e) {
            error = e.getMessage();
        }
        Assert.assertEquals("", error);
    }

    @Test
    public void checkDeletePerson_NotFound() {
        when(personRepository.findById(1L)).thenReturn(Optional.empty());

        String error = "";
        try {
            personService.deletePerson(1L);
        } catch (Exception e) {
            error = e.getClass().getSimpleName();
        }
        Assert.assertEquals("NotFoundException", error);
    }


    @Test
    public void checkSearchPerson() {
        when(personRepository.findById(1L)).thenReturn(Optional.of(person1));

        String error = "";
        try {
            personService.searchPerson(1L);
        } catch (Exception e) {
            error = e.getMessage();
        }
        Assert.assertEquals("", error);
    }

    @Test
    public void checkSearchPerson_NotFound() {
        when(personRepository.findById(1L)).thenReturn(Optional.empty());

        String error = "";
        try {
            personService.deletePerson(1L);
        } catch (Exception e) {
            error = e.getClass().getSimpleName();
        }
        Assert.assertEquals("NotFoundException", error);
    }

    @Test
    public void checkSearchPersons_Page0() {
        when(personRepository.findAll((Specification) isNull(), any(Pageable.class))).thenReturn(personsPage0);

        Page<PersonEntity> pagePersons = personService.searchPersons(null, null, null,
                null, null, 0, 2, "name", "asc");

        Assert.assertEquals(4, pagePersons.getTotalElements());
        Assert.assertEquals(2, pagePersons.getSize());
        Assert.assertEquals("bruna", pagePersons.getContent().get(0).getName());
        Assert.assertEquals("francisco", pagePersons.getContent().get(1).getName());
    }

    @Test
    public void checkSearchPersons_Page1() {
        when(personRepository.findAll((Specification) isNull(), any(Pageable.class))).thenReturn(personsPage1);

        Page<PersonEntity> pagePersons = personService.searchPersons(null, null, null,
                null, null, 1, 2, "name", "asc");

        Assert.assertEquals(4, pagePersons.getTotalElements());
        Assert.assertEquals(2, pagePersons.getSize());
        Assert.assertEquals("xunaymes", pagePersons.getContent().get(0).getName());
        Assert.assertEquals("zeze", pagePersons.getContent().get(1).getName());
    }

    @Test
    public void checkSearchPersons_orderSex() {
        when(personRepository.findAll((Specification) isNull(), any(Pageable.class))).thenReturn(personsPageOrderSex);

        Page<PersonEntity> pagePersons = personService.searchPersons(null, null, null,
                null, null, 0, 2, "sex", "desc");

        Assert.assertEquals(4, pagePersons.getTotalElements());
        Assert.assertEquals(2, pagePersons.getSize());
        Assert.assertEquals("zeze", pagePersons.getContent().get(0).getName());
        Assert.assertEquals("francisco", pagePersons.getContent().get(1).getName());
    }


    @Test
    public void checkSearchPersons_filterName() {
        when(personRepository.findAll(any(Specification.class), any(Pageable.class))).thenReturn(personsPageFilterName);

        Page<PersonEntity> pagePersons = personService.searchPersons("una", null, null,
                null, null, 0, 2, "name", "asc");

        Assert.assertEquals(2, pagePersons.getTotalElements());
        Assert.assertEquals(2, pagePersons.getSize());
        Assert.assertEquals("bruna", pagePersons.getContent().get(0).getName());
        Assert.assertEquals("xunaymes", pagePersons.getContent().get(1).getName());
    }

}
