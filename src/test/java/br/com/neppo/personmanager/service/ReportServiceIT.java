package br.com.neppo.personmanager.service;

import br.com.neppo.personmanager.entity.PersonEntity;
import br.com.neppo.personmanager.model.Report;
import br.com.neppo.personmanager.model.SexType;
import br.com.neppo.personmanager.repository.PersonRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class ReportServiceIT {

    private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private static boolean initiate = false;
    private static List<PersonEntity> persons;

    @Mock
    private PersonRepository personRepository;

    @InjectMocks
    private ReportService reportService;


    @Before
    public void initPersons() throws ParseException {
        if (!initiate) {
            PersonEntity p1 = new PersonEntity(1L, "p1", "0192000023", dateFormat.parse("01/08/2009"), SexType.MALE, "Rua uno, 1");
            PersonEntity p2 = new PersonEntity(2L, "p2", "0192222223", dateFormat.parse("01/08/2008"), SexType.FEMALE, "Rua dois, 22");
            PersonEntity p3 = new PersonEntity(3L, "p3", "0199991023", dateFormat.parse("01/08/1957"), SexType.MALE, "Rua tres, 333");
            PersonEntity p4 = new PersonEntity(4L, "p4", "1192391023", dateFormat.parse("01/08/1990"), SexType.FEMALE, "Rua quatro, 44");
            persons = Arrays.asList(p1, p2, p3, p4);

            initiate = true;
        }
    }

    @Test
    public void checkReportAge() {
        when(personRepository.findAll()).thenReturn(persons);

        Report report = reportService.generateReportAges();
        Assert.assertEquals(1L, report.getRecords().get("0 a 9").longValue());
        Assert.assertEquals(1L, report.getRecords().get("10 a 19").longValue());
        Assert.assertEquals(1L, report.getRecords().get("20 a 29").longValue());
        Assert.assertEquals(0L, report.getRecords().get("30 a 39").longValue());
        Assert.assertEquals(1L, report.getRecords().get("Maior que 40").longValue());
    }

    @Test
    public void checkReportSex() {
        when(personRepository.findAll()).thenReturn(persons);

        Report report = reportService.generateReportSex();
        Assert.assertEquals(2L, report.getRecords().get("Masculino").longValue());
        Assert.assertEquals(2L, report.getRecords().get("Feminino").longValue());
    }
}
