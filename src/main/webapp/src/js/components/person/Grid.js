import React, { Component } from 'react';
import { connect } from 'react-redux'
import { searchPerson, searchPersonSuccess, searchPersonFailure,
  removePerson, removePersonSuccess, removePersonFailure, } from '../../actions/person-action';

const mapStateToProps = state => {
  return {
    person: state.person
  };
}

const mapDispatchToProps = dispatch => {
  return {
    searchPerson: (personSearch) => {
      const result = dispatch(searchPerson(personSearch));
      result.payload.then(response => {
        response.status === 200 ? dispatch(searchPersonSuccess(response.data)) : dispatch(searchPersonFailure(response.data));
      }).catch(error => {
        dispatch(searchPersonFailure(error));
      });
    },
    removePerson: (id) => {
      const result = dispatch(removePerson(id));
      result.payload.then(response => {
        response.status === 200 ? dispatch(removePersonSuccess(response.data)) : dispatch(removePersonFailure(response.data));
      }).catch(error => {
        dispatch(removePersonFailure(error));
      });
    }
  }
}

class ConnectedGrid extends Component {
  constructor() {
    super();
    this.state = {
      personSearch: {
        name: '',
        document: '',
        birthdate: '',
        sex: '',
        address: '',
        pageSize: '10',
        page: '0',
        orderField: 'name',
        order: 'asc'
      }
    };
    this.handleChange = this.handleChange.bind(this);
    this.paginationClick = this.paginationClick.bind(this);
  }

  paginationClick(value){
    let personSearch = this.state.personSearch;
    personSearch.page = value;
    this.setState({personSearch: personSearch});
    this.props.searchPerson(personSearch);
  }

  removeClick(value) {
    if (window.confirm('Are you sure you wish to remove this person ('+value+')?')){
      this.props.removePerson(value);
      this.props.searchPerson(this.state.personSearch);
    }
  }

  handleChange(event) {
    let field = event.target.id;
    let personSearch = this.state.personSearch;
    if(field === 'name') {
      personSearch.name = event.target.value;
    }
    if(field === 'document') {
      personSearch.document = event.target.value;
    }
    if(field === 'birthdate') {
      personSearch.birthdate = event.target.value;
    }
    if(field === 'sex') {
      personSearch.sex = event.target.value;
    }
    if(field === 'address') {
      personSearch.address = event.target.value;
    }
    if(field === 'pageSize') {
      personSearch.pageSize = event.target.value;
      personSearch.page = 0;
    }
    this.setState({personSearch: personSearch});
    this.props.searchPerson(personSearch);
  }

  componentWillMount() {
    this.props.searchPerson();
  }

  renderLoading() {
    return (<h5>Loading...</h5>);
  }

  renderError(error) {
    return (<div className="alert alert-danger">Error: {error.message}</div>);
  }

  renderHeader() {
    return (
      <div>
        <div className="row justify-content-end">
          <a href="/person" className="btn btn-secondary btn-sm justify-content-center disabled">Back</a>
            |
          <a href="/person/add" className="btn btn-primary btn-sm justify-content-center">New Person</a>
        </div>
        <h3>People Grid</h3>
      </div>
    );
  }

  renderPagination(nroPages, actualPage){
    let pages = [];
    for (let i=0 ; i < nroPages ; i++) {
      actualPage === i ?
        pages.push(<li className="page-item active"><a className="page-link" id="page">{i+1}</a></li>) :
        pages.push(<li className="page-item"><a className="page-link" id="page" onClick={() => this.paginationClick(i)} value={i+1}>{i+1}</a></li>);
    }
    return pages;
  }

  renderGrid(personPage) {
      return (
        <div>
        <div className="row justify-content-end">
          <div className="col-sm-0">
            <select className="form-control form-control-sm" id="pageSize" onChange={this.handleChange} value={this.state.personSearch.pageSize}>
              <option value="2">2 per page</option>
              <option value="5">5 per page</option>
              <option value="10">10 per page</option>
              <option value="20">20 per page</option>
              <option value="50">50 per page</option>
            </select>
          </div>
        </div>
        <br />
        <table className="table table-hover">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Document</th>
              <th scope="col">BirthDate</th>
              <th scope="col">Sex</th>
              <th scope="col">Address</th>
              <th scope="col">Actions</th>
            </tr>
          </thead>
          <tbody>
           {personPage.content.map(p => (
              <tr key={p.id}>
                <th scope="row">{p.name}</th>
                <td>{p.document}</td>
                <td>{p.birthdate}</td>
                <td>{p.sex}</td>
                <td>{p.address}</td>
                <td>
                  <a href={'/person/edit/'+ p.id}>Edit</a> / <a href="" onClick={() => this.removeClick(p.id)}>Remove</a>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        <nav >
          <ul className="pagination justify-content-center pagination-sm">
            {personPage.first === true ? (
              <li className="page-item disabled" ><a className="page-link">Previous</a></li>
            ) : (
              <li className="page-item" ><a className="page-link" onClick={() => this.paginationClick(personPage.number - 1)}>Previous</a></li>
            ) }
            {this.renderPagination(personPage.totalPages, personPage.number)}
            {personPage.last === true ? (
              <li className="page-item disabled" ><a className="page-link">Next</a></li>
            ) : (
              <li className="page-item" ><a className="page-link" onClick={() => this.paginationClick(personPage.number + 1)}>Next</a></li>
            ) }
          </ul>
        </nav></div>
      );
  }

  render() {
    const { personPage, loading, error } = this.props.person;
    if(loading) {
      return (
        <div className="container">
          {this.renderHeader()}
          {this.renderLoading()}
        </div>
      );
    } else if(error) {
      return (
        <div className="container">
          {this.renderHeader()}
          {this.renderError(error)}
        </div>
      );
    }
    return (
      <div className="container">
        {this.renderHeader()}
        {this.renderGrid(personPage)}
      </div>
    );
  }
}

const Grid = connect(mapStateToProps, mapDispatchToProps)(ConnectedGrid);
export default Grid;
