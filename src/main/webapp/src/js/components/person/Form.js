import React, { Component } from "react";
import { connect } from "react-redux";
import { createPerson, createPersonSuccess, createPersonFailure,
findPerson, findPersonSuccess, findPersonFailure } from '../../actions/person-action';

const mapStateToProps = state => {
  return {
    person: state.person
  };
}

const mapDispatchToProps = dispatch => {
  return {
    createPerson: (person) => {
      const result = dispatch(createPerson(person));
      result.payload.then(response => {
        response.status === 201 || response.status === 200  ? dispatch(createPersonSuccess(response.data)) : dispatch(createPersonFailure(response.data));
      }).catch(error => {
        dispatch(createPersonFailure(error));
      });
    },
    findPerson: (id) => {
      const result = dispatch(findPerson(id));
      result.payload.then(response => {
        response.status === 200 ? dispatch(findPersonSuccess(response.data)) : dispatch(findPersonFailure(response.data));
      }).catch(error => {
        dispatch(findPersonFailure(error));
      });
    }
  }
}

class ConnectedForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      person: {
        id: props.routeParams.id,
        name: '',
        document: '',
        birthdate: '',
        sex: 'MALE',
        address: ''
      },
      loadFields: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount() {
    if(this.state.person.id !== undefined) {
      this.props.findPerson(this.state.person.id);
    }
  }

  handleChange(event) {
    let field = event.target.id;
    let person = this.state.person;
    if(field === 'name') {
      person.name = event.target.value;
    }
    if(field === 'document') {
      person.document = event.target.value;
    }
    if(field === 'birthdate') {
      person.birthdate = event.target.value;
    }
    if(field === 'sex') {
      person.sex = event.target.value;
    }
    if(field === 'address') {
      person.address = event.target.value;
    }
    this.setState({person: person});
  }

  handleSubmit(event) {
    event.preventDefault();
    if(this.state.person.name === '' ||
      this.state.person.document === '' ||
      this.state.person.birthdate === '') {
        window.alert("The fields: 'Name', 'Document', 'Birthdate' and 'Sex' are required")
    } else{
        this.props.createPerson(this.state.person);
    }
  }

  loadFields(editPerson){
    if(!this.state.loadFields){
      this.setState({person: editPerson});
      this.setState({loadFields: true});
    }
  }

  renderLoading() {
    return (<h5>Loading...</h5>);
  }

  renderSucess(error) {
    setTimeout(function() {
        window.location = '/person';
    }.bind(this), 1500);
    return (<div className="alert alert-success">Success, person {this.state.person.id === undefined ? 'created' : 'updated'}!</div>);
  }

  renderError(error) {
    return (<div className="alert alert-danger">Error: {error.message}</div>);
  }

  renderHeader() {
    return (
      <div>
        <div className="row justify-content-end">
          <a href="/person" className="btn btn-primary btn-sm justify-content-center ">Back</a>
            |
          <a href="/person/add" className="btn btn-secondary btn-sm justify-content-center disabled">New Person</a>
        </div>
        <h3>{this.state.person.id === undefined ? 'New Person' : 'Update Person'}</h3><br />
      </div>
    );
  }

  renderForm() {
    return (
      <form onSubmit={this.handleSubmit}>
        <div className="form-group row">
          <label htmlFor="name" className="col-sm-2 col-form-label-sm">*Name</label>
          <div className="col-sm-10">
            <input type="text" className="form-control form-control-sm" id="name" value={this.state.person.name} onChange={this.handleChange} />
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="document" className="col-sm-2 col-form-label-sm">*Document</label>
          <div className="col-sm-10">
            <input type="text" className="form-control form-control-sm" id="document" value={this.state.person.document} onChange={this.handleChange} />
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="birthdate" className="col-sm-2 col-form-label-sm">*Birthdate</label>
          <div className="col-sm-10">
            <input type="text" className="form-control form-control-sm" id="birthdate" value={this.state.person.birthdate} onChange={this.handleChange} />
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="sex" className="col-sm-2 col-form-label-sm">*Sex:</label>
          <div className="col-sm-10">
            <select className="form-control form-control-sm" id="sex" value={this.state.person.sex} onChange={this.handleChange}>
              <option value="MALE">MALE</option>
              <option value="FEMALE">FEMALE</option>
            </select>
          </div>
        </div>
        <div className="form-group row">
          <label htmlFor="address" className="col-sm-2 col-form-label-sm">Address:</label>
          <div className="col-sm-10">
            <input type="text" className="form-control form-control-sm" id="address" value={this.state.person.address} onChange={this.handleChange} />
          </div>
        </div>
        <div className="row justify-content-end">
          <button type="submit" className="btn btn-success btn-sm">
            {this.state.person.id === undefined ? 'Create' : 'Update'}
          </button>
        </div>
      </form>
    );
  }

  render() {
    const { loading, error, person, created } = this.props.person;
    if(created) {
      return (
        <div className="container">
        {this.renderHeader()}
        {this.renderSucess()}
        {this.renderForm()}
        </div>
      );
    }
    if(loading) {
      return (
        <div className="container">
          {this.renderHeader()}
          {this.renderLoading()}
          {this.renderForm()}
        </div>
      );
    } else if(error) {
      return (
        <div className="container">
          {this.renderHeader()}
          {this.renderError(error)}
          {this.renderForm()}
        </div>
      );
    }
    if(person){
      this.loadFields(person);
    }
    return (
      <div className="container">
        {this.renderHeader()}
        {this.renderForm()}
      </div>
    );
  }

}
const Form = connect(mapStateToProps, mapDispatchToProps)(ConnectedForm);
export default Form;
