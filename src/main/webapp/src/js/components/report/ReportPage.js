import React, { Component } from 'react';
import { connect } from 'react-redux'
import { ageReport, ageReportSuccess, ageReportFailure,
  sexReport, sexReportSuccess, sexReportFailure, } from '../../actions/report-action';
import Chart from "react-google-charts";

const mapStateToProps = state => {
  return {
    report: state.report
  };
}

const mapDispatchToProps = dispatch => {
  return {
    ageReport: () => {
      const result = dispatch(ageReport());
      result.payload.then(response => {
        response.status === 200 ? dispatch(ageReportSuccess(response.data)) : dispatch(ageReportFailure(response.data));
      }).catch(error => {
        dispatch(ageReportFailure(error));
      });
    },
    sexReport: () => {
      const result = dispatch(sexReport());
      result.payload.then(response => {
        response.status === 200 ? dispatch(sexReportSuccess(response.data)) : dispatch(sexReportFailure(response.data));
      }).catch(error => {
        dispatch(sexReportFailure(error));
      });
    }
  }
}

class ConnectedReportPage extends Component {
	constructor (){
		super();
		this.state = {
			ageGraphRender: false,
			sexGraphRender: false
		}
	}

	componentWillMount() {
		this.props.ageReport();
		this.props.sexReport();
	}

	renderGraphAge(records) {
		let data = [];
		data.push(['Chave', 'Person Ages'])
		Object.keys(records).map(function(key) {
			let record = [];
			record.push(key);
			record.push(records[key]);
			data.push(record);
			return 1;
		});
    return (
    	<Chart chartType="ColumnChart" width="100%" height="350px" data={data} />
    );
  }

	renderGraphSex(records) {
		let data = [];
		data.push(['Chave', 'Person Sex'])
		Object.keys(records).map(function(key) {
			let record = [];
			record.push(key);
			record.push(records[key]);
			data.push(record);
			return 1;
		});
		//this.setState({sexGraphRender: true});
    return (
    	<Chart chartType="PieChart" width="100%" height="350px" data={data} />
    );
  }

	render() {
		const { loading, error, ageReport, sexReport } = this.props.report;
		return (
			<div className="container">
			  <div className="row">
					<div className="col">
			     	<h3>Age Report</h3>
						{loading ? (<h5>Loading...</h5>) : ''}
						{error ? (<div className="alert alert-danger">Error: {error.message}</div>) : ''}
						{ageReport && sexReport ? this.renderGraphAge(ageReport.records) : ''}
			    </div>
			    <div className="col">
			      <h3>Sex Report</h3>
						{loading ? (<h5>Loading...</h5>) : ''}
						{error ? (<div className="alert alert-danger">Error: {error.message}</div>) : ''}
						{ageReport && sexReport ? this.renderGraphSex(sexReport.records) : ''}
			    </div>
			  </div>
			</div>
		);
	}
}

const ReportPage = connect(mapStateToProps, mapDispatchToProps)(ConnectedReportPage);
export default ReportPage;
