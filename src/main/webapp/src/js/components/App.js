import React, { Component } from 'react';
import PersonForm from "./person/Form";
import PersonGrid from "./person/Grid";
import ReportPage from "./report/ReportPage";

import {Router,Route,browserHistory} from 'react-router';

class App extends Component {
	render() {
		return(
			<div>
			  <div className="jumbotron jumbotron-fluid text-center">
			    <h1>Person Management</h1>
			    <p>Search, create, edit, and remove any person.</p>
					<h3><ul className="nav justify-content-center">
					  <li className="nav-item">
					    <a className={window.location.href.indexOf('/person') !== -1 ? 'nav-link disabled': 'nav-link'} href="/person">People</a>
					  </li>
					  <li className="nav-item">
					    <a className={window.location.href.indexOf('/report') !== -1 ? 'nav-link disabled': 'nav-link'} href="/report">Reports</a>
					  </li>
					</ul></h3>
			  </div>
				<Router history={browserHistory}>
		        <Route path="/person" component={PersonGrid} />
		        <Route path="/person/add" component={PersonForm} />
		        <Route path="/person/edit/:id" component={PersonForm} />
						<Route path="/report" component={ReportPage} />
		    </Router>
		  </div>
		);
	}
}

export default App;
