import axios from 'axios';
import {
	AGE_REPORT, AGE_REPORT_SUCCESS, AGE_REPORT_FAILURE,
	SEX_REPORT, SEX_REPORT_SUCCESS, SEX_REPORT_FAILURE
} from '../constants/action-types';

const ROOT_URL = 'http://localhost:8080';

export function ageReport() {
  const request = axios({
    method: 'get',
    url: `${ROOT_URL}/report/ages`,
    headers: []
  });
  return {
    type: AGE_REPORT,
    payload: request
  };
}

export function ageReportSuccess(report) {
  return {
    type: AGE_REPORT_SUCCESS,
    payload: report
  };
}

export function ageReportFailure(error) {
  return {
    type: AGE_REPORT_FAILURE,
    payload: error
  };
}

export function sexReport() {
  const request = axios({
    method: 'get',
    url: `${ROOT_URL}/report/sex`,
    headers: []
  });
  return {
    type: SEX_REPORT,
    payload: request
  };
}

export function sexReportSuccess(report) {
  return {
    type: SEX_REPORT_SUCCESS,
    payload: report
  };
}

export function sexReportFailure(error) {
  return {
    type: SEX_REPORT_FAILURE,
    payload: error
  };
}
