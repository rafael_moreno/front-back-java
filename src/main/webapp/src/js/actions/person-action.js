import axios from 'axios';
import {
	SEARCH_PERSON, SEARCH_PERSON_SUCCESS, SEARCH_PERSON_FAILURE,
	FIND_PERSON, FIND_PERSON_SUCCESS, FIND_PERSON_FAILURE,
	CREATE_PERSON, CREATE_PERSON_SUCCESS, CREATE_PERSON_FAILURE,
	REMOVE_PERSON, REMOVE_PERSON_SUCCESS, REMOVE_PERSON_FAILURE
} from '../constants/action-types';

const ROOT_URL = 'http://localhost:8080';

export function searchPerson(personSearch) {
	var parameters = personSearch === undefined ?
		'' : '?page='+personSearch.page +
		'&pageSize='+personSearch.pageSize ;
  const request = axios({
    method: 'get',
    url: `${ROOT_URL}/person`+parameters,
    headers: []
  });
  return {
    type: SEARCH_PERSON,
    payload: request
  };
}

export function searchPersonSuccess(people) {
  return {
    type: SEARCH_PERSON_SUCCESS,
    payload: people
  };
}

export function searchPersonFailure(error) {
  return {
    type: SEARCH_PERSON_FAILURE,
    payload: error
  };
}

export function findPerson(id) {
  const request = axios({
    method: 'get',
    url: `${ROOT_URL}/person/`+id,
    headers: []
  });
  return {
    type: FIND_PERSON,
    payload: request
  };
}

export function findPersonSuccess(people) {
  return {
    type: FIND_PERSON_SUCCESS,
    payload: people
  };
}

export function findPersonFailure(error) {
  return {
    type: FIND_PERSON_FAILURE,
    payload: error
  };
}

export function createPerson(person) {
	const method =  person.id === undefined ? 'put': 'post';
	const parameter =  person.id === undefined ? '': '/'+person.id;
  const request = axios({
    method: method,
    data: person,
    url: `${ROOT_URL}/person`+parameter,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  });
  return {
    type: CREATE_PERSON,
    payload: request
  };
}

export function createPersonSuccess(newPerson) {
  return {
    type: CREATE_PERSON_SUCCESS,
    payload: newPerson
  };
}

export function createPersonFailure(error) {
  return {
    type: CREATE_PERSON_FAILURE,
    payload: error
  };
}

export function removePerson(id) {
  const request = axios({
    method: 'delete',
    url: `${ROOT_URL}/person/`+id,
    headers: []
  });
  return {
    type: REMOVE_PERSON,
    payload: request
  };
}

export function removePersonSuccess(removedPerson) {
  return {
    type: REMOVE_PERSON_SUCCESS,
    payload: removedPerson
  };
}

export function removePersonFailure(error) {
  return {
    type: REMOVE_PERSON_FAILURE,
    payload: error
  };
}
