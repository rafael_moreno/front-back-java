import {
	SEARCH_PERSON, SEARCH_PERSON_SUCCESS, SEARCH_PERSON_FAILURE,
	FIND_PERSON, FIND_PERSON_SUCCESS, FIND_PERSON_FAILURE,
	CREATE_PERSON, CREATE_PERSON_SUCCESS, CREATE_PERSON_FAILURE,
  REMOVE_PERSON, REMOVE_PERSON_SUCCESS, REMOVE_PERSON_FAILURE
} from '../constants/action-types';

const INITIAL_STATE = {
  personPage: {
    content: []
  },
	created: false,
  error: null,
  loading: false,
	person: null
};

export default function(state = INITIAL_STATE, action) {
  let error;
  switch(action.type) {

  case SEARCH_PERSON:
  	return { ...state,  personPage:[], error: null, loading: true  };
  case SEARCH_PERSON_SUCCESS:
    return { ...state, personPage: action.payload, error: null, loading: false };
  case SEARCH_PERSON_FAILURE:
    error = action.payload.error || {message: action.payload.message};
    return { ...state, personPage: [], error: error, loading: false };

	case FIND_PERSON:
  	return { ...state, person: null, error: null, loading: true  };
  case FIND_PERSON_SUCCESS:
    return { ...state, person: action.payload, error: null, loading: false };
  case FIND_PERSON_FAILURE:
    error = action.payload.error || {message: action.payload.message};
    return { ...state, person: null, error: error, loading: false };

  case CREATE_PERSON:
  	return {...state, created: false, error: null, loading: true }
  case CREATE_PERSON_SUCCESS:
  	return {...state, created: true, error: null, loading: false }
  case CREATE_PERSON_FAILURE:
    error = action.payload.error || {message: action.payload.message};
  	return {...state, created: false, error: error, loading: false }

	case REMOVE_PERSON:
  	return {...state, error: null, loading: true }
  case REMOVE_PERSON_SUCCESS:
  	return {...state, error: null, loading: false }
  case REMOVE_PERSON_FAILURE:
    error = action.payload.error || {message: action.payload.message};
  	return {...state, error: error, loading: false }

  default:
    return state;
  }
}
