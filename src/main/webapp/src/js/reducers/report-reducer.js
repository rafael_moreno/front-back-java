import {
	AGE_REPORT, AGE_REPORT_SUCCESS, AGE_REPORT_FAILURE,
	SEX_REPORT, SEX_REPORT_SUCCESS, SEX_REPORT_FAILURE
} from '../constants/action-types';

const INITIAL_STATE = {
	error: null,
	loading: false,
  ageReport: null,
  sexReport: null
};

export default function(state = INITIAL_STATE, action) {
  let error;
  switch(action.type) {

  case AGE_REPORT:
  	return { ...state, ageReport: null, error: null, loading: true };
  case AGE_REPORT_SUCCESS:
    return { ...state, ageReport: action.payload, error: null, loading: false };
  case AGE_REPORT_FAILURE:
    error = action.payload.error || {message: action.payload.message};
    return { ...state, ageReport: null, error: error, loading: false };

  case SEX_REPORT:
  	return { ...state, sexReport: null, error: null, loading: true };
  case SEX_REPORT_SUCCESS:
    return { ...state, sexReport: action.payload, error: null, loading: false };
  case SEX_REPORT_FAILURE:
    error = action.payload.error || {message: action.payload.message};
    return { ...state, sexReport: null, error: error, loading: false };

  default:
    return state;
  }
}
