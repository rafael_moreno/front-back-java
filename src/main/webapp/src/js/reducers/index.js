import { combineReducers } from 'redux';
import PersonReducer from './person-reducer';
import ReportReducer from './report-reducer';

const rootReducer = combineReducers({
  person: PersonReducer,
  report: ReportReducer
});

export default rootReducer;
