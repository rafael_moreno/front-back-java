insert into person (id, name, document, birthdate, sex, address) values (1, 'Rafão Moreno da Silva Novo', '09219966601', '1990-03-23', 'MALE', 'Rua xambrolho, 123');
insert into person (id, name, document, birthdate, sex, address) values (2, 'Francisco de Maria',         '03219961601', '1950-01-30', 'MALE', 'Avenida das abobrinhas, 3290');
insert into person (id, name, document, birthdate, sex, address) values (3, 'Vaniscreide Luzia',          '05219756601', '1970-05-28', 'FEMALE', null);
insert into person (id, name, document, birthdate, sex, address) values (4, 'Creusa da Silva',            '09334146601', '1994-06-12', 'FEMALE', null);
insert into person (id, name, document, birthdate, sex, address) values (5, 'Dona Rita de Cassia',        '11239966601', '2010-02-04', 'FEMALE', 'Rua quinze, 13');
insert into person (id, name, document, birthdate, sex, address) values (6, 'Zelton Nascimento',          '03440966601', '2001-09-07', 'MALE', null);
insert into person (id, name, document, birthdate, sex, address) values (7, 'Bizon Cristiano da Silva',   '09254677891', '1982-10-11', 'MALE', null);
insert into person (id, name, document, birthdate, sex, address) values (8, 'Xupeta Nazaré',              '02343712481', '1979-12-24', 'MALE', null);