package br.com.neppo.personmanager.entity;

import br.com.neppo.personmanager.model.SexType;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "person")
public class PersonEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", length = 255, nullable = false)
    private String name;

    @Column(name = "document", length = 19, nullable = false)
    private String document;

    @Column(name = "birthdate", nullable = false)
    @JsonFormat(pattern = "dd/MM/yyyy")
    private Date birthdate;

    @Column(name = "sex", length = 10, nullable = false)
    @Enumerated(EnumType.STRING)
    private SexType sex;

    @Column(name = "address", length = 2000, nullable = true)
    private String address;

    public PersonEntity() {
    }

    public PersonEntity(Long id, String name, String document, Date birthdate, SexType sex, String address) {
        this.id = id;
        this.name = name;
        this.document = document;
        this.birthdate = birthdate;
        this.sex = sex;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public String getDocument() {
        return document;
    }

    public SexType getSex() {
        return sex;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public String toString() {
        return "PersonEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", birthdate=" + birthdate +
                ", document='" + document + '\'' +
                ", sex=" + sex.getValue() +
                ", address='" + address + '\'' +
                '}';
    }
}
