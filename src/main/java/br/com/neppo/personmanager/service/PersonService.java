package br.com.neppo.personmanager.service;

import br.com.neppo.personmanager.entity.PersonEntity;
import br.com.neppo.personmanager.exception.AlreadyExistsException;
import br.com.neppo.personmanager.exception.NotFoundException;
import br.com.neppo.personmanager.model.SexType;
import br.com.neppo.personmanager.repository.PersonRepository;
import br.com.neppo.personmanager.repository.SearchSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public PersonEntity saveNewPerson(PersonEntity person) throws AlreadyExistsException {
        PersonEntity existingPerson = personRepository.findByDocument(person.getDocument());
        if (existingPerson != null) {
            throw new AlreadyExistsException(existingPerson.toString());
        }
        return personRepository.save(person);
    }

    public Page<PersonEntity> searchPersons(String name, String document, Date birthdate,
                                            SexType sex, String address, Integer page, Integer pageSize,
                                            String orderField, String order) {

        Sort sort = new Sort("asc".equalsIgnoreCase(order) ?
                Sort.Direction.ASC : Sort.Direction.DESC, orderField);
        Pageable pageable = PageRequest.of(page, pageSize, sort);

        return personRepository.findAll(SearchSpecification.filterQuery(name, document,
                birthdate, sex, address), pageable);
    }

    public PersonEntity searchPerson(Long personId) throws NotFoundException {
        Optional<PersonEntity> resultPerson = personRepository.findById(personId);
        if (!resultPerson.isPresent()) {
            throw new NotFoundException();
        }
        return resultPerson.get();
    }

    public PersonEntity updatePerson(Long targetPersonId, PersonEntity person) throws NotFoundException {
        if (!personRepository.findById(targetPersonId).isPresent()) {
            throw new NotFoundException();
        }
        PersonEntity newPerson = new PersonEntity(targetPersonId, person.getName(),
                person.getDocument(), person.getBirthdate(), person.getSex(), person.getAddress());
        return personRepository.save(newPerson);
    }

    public void deletePerson(Long targetPersonId) throws NotFoundException {
        Optional<PersonEntity> resultPerson = personRepository.findById(targetPersonId);
        if (!resultPerson.isPresent()) {
            throw new NotFoundException();
        }
        personRepository.delete(resultPerson.get());
    }

}
