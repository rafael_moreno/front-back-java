package br.com.neppo.personmanager.service;

import br.com.neppo.personmanager.entity.PersonEntity;
import br.com.neppo.personmanager.model.Report;
import br.com.neppo.personmanager.model.SexType;
import br.com.neppo.personmanager.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service
public class ReportService {

    private static final String[] labelsAge = {"0 a 9", "10 a 19", "20 a 29", "30 a 39", "Maior que 40"};

    @Autowired
    private PersonRepository personRepository;


    public Report generateReportAges() {
        List<PersonEntity> persons = personRepository.findAll();
        final Integer[] countAgeRange = {0, 0, 0, 0, 0};
        persons.stream().forEach(person -> {
            Integer age = calculeAge(person.getBirthdate()).intValue();
            Integer ageRange = age / 10;
            if (ageRange > 4) {
                ageRange = 4;
            }
            countAgeRange[ageRange]++;
        });
        HashMap<String, Integer> records = new HashMap<>();
        for (int i = 0; i < 5; i++) {
            records.put(labelsAge[i], countAgeRange[i]);
        }
        return new Report(records);
    }

    public Report generateReportSex() {
        List<PersonEntity> persons = personRepository.findAll();
        final Integer[] countSex = {0, 0};
        persons.stream().forEach(person -> {
            if (SexType.MALE.equals(person.getSex())) {
                countSex[0]++;
            } else {
                if (SexType.FEMALE.equals(person.getSex())) {
                    countSex[1]++;
                }
            }
        });
        HashMap<String, Integer> records = new HashMap<>();
        records.put(SexType.MALE.getValue(), countSex[0]);
        records.put(SexType.FEMALE.getValue(), countSex[1]);
        return new Report(records);
    }

    private Long calculeAge(Date birthDate) {
        LocalDate begin = birthDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate end = new Date().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return ChronoUnit.YEARS.between(begin, end);
    }

}
