package br.com.neppo.personmanager.model;

public enum SexType {
    MALE("Masculino"),
    FEMALE("Feminino");

    private String value;

    SexType(String value) {
        this.value = value;
    }

    public static SexType fromString(String sex) {
        for (SexType s : SexType.values()) {
            if (s.toString().equalsIgnoreCase(sex)) {
                return s;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }
}
