package br.com.neppo.personmanager.model;

import java.util.Map;

public class Report {

    private Map<String, Integer> records;

    public Report(Map<String, Integer> records) {
        this.records = records;
    }

    public Map<String, Integer> getRecords() {
        return records;
    }

    @Override
    public String toString() {
        return "Report{" +
                "records=" + records +
                '}';
    }
}
