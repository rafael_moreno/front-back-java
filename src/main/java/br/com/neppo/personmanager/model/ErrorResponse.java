package br.com.neppo.personmanager.model;

public class ErrorResponse {
    private String error;
    private String statusCode;

    public ErrorResponse(String error, String statusCode) {
        this.error = error;
        this.statusCode = statusCode;
    }

    public String getError() {
        return error;
    }

    public String getStatusCode() {
        return statusCode;
    }
}
