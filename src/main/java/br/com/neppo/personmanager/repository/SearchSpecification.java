package br.com.neppo.personmanager.repository;

import br.com.neppo.personmanager.model.SexType;
import org.springframework.data.jpa.domain.Specification;

import java.util.Date;

public class SearchSpecification {

    private SearchSpecification() {
        throw new IllegalStateException("Utility class");
    }

    public static Specification filterQuery(String name, String document, Date birthdate,
                                            SexType sex, String address) {
        Specification specification = null;

        if (name != null && !name.isEmpty()) {
            specification = Specification.where((root, query, cb) -> cb.like(root.get("name"), '%' + name + '%'));
        }
        if (document != null && !document.isEmpty()) {
            specification = specification == null ? Specification.where((root, query, cb) -> cb.like(root.get("document"), '%' + document + '%'))
                    : specification.and((root, query, cb) -> cb.like(root.get("document"), '%' + document + '%'));
        }
        if (birthdate != null) {
            specification = specification == null ? Specification.where((root, query, cb) -> cb.equal(root.get("birthdate"), birthdate))
                    : specification.and((root, query, cb) -> cb.equal(root.get("birthdate"), birthdate));
        }
        if (sex != null) {
            specification = specification == null ? Specification.where((root, query, cb) -> cb.equal(root.get("sex"), sex))
                    : specification.and((root, query, cb) -> cb.equal(root.get("sex"), sex));
        }
        if (address != null && !address.isEmpty()) {
            specification = specification == null ? Specification.where((root, query, cb) -> cb.like(root.get("address"), '%' + address + '%'))
                    : specification.and((root, query, cb) -> cb.like(root.get("address"), '%' + address + '%'));
        }

        return specification;
    }
}
