package br.com.neppo.personmanager.controller;

import br.com.neppo.personmanager.model.ErrorResponse;
import br.com.neppo.personmanager.model.Report;
import br.com.neppo.personmanager.service.ReportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ReportController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ReportService reportService;

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/report/ages", method = RequestMethod.GET)
    public ResponseEntity reportAges() {
        try {
            Report report = reportService.generateReportAges();
            logger.info("Report generated. {}", report);
            return new ResponseEntity(report, HttpStatus.OK);
        } catch (Exception e) {
            return returnError("Unexpected error to generate report ages, contact an administrator", HttpStatus.INTERNAL_SERVER_ERROR, e);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/report/sex", method = RequestMethod.GET)
    public ResponseEntity reportSex() {
        try {
            Report report = reportService.generateReportSex();
            logger.info("Report generated. {}", report);
            return new ResponseEntity(report, HttpStatus.OK);
        } catch (Exception e) {
            return returnError("Unexpected error to generate report sex, contact an administrator", HttpStatus.INTERNAL_SERVER_ERROR, e);
        }
    }

    private ResponseEntity returnError(String errorMsg, HttpStatus status, Exception exception) {
        if (exception == null) {
            logger.error(errorMsg);
        } else {
            logger.error(errorMsg, exception);
        }
        ErrorResponse errorResponse = new ErrorResponse(errorMsg, status.toString());
        return new ResponseEntity(errorResponse, status);
    }
}
