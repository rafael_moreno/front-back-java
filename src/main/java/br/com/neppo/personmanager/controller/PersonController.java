package br.com.neppo.personmanager.controller;

import br.com.neppo.personmanager.entity.PersonEntity;
import br.com.neppo.personmanager.exception.AlreadyExistsException;
import br.com.neppo.personmanager.exception.NotFoundException;
import br.com.neppo.personmanager.model.ErrorResponse;
import br.com.neppo.personmanager.model.SexType;
import br.com.neppo.personmanager.service.PersonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;

@Controller
public class PersonController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private PersonService personService;

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/person", method = RequestMethod.PUT)
    public ResponseEntity createPerson(@RequestBody PersonEntity person) {
        if (person == null || person.getName() == null || person.getDocument() == null ||
                person.getBirthdate() == null || person.getSex() == null) {
            return returnError("Error to create person, required fields: name, document, birthdate and sex", HttpStatus.UNPROCESSABLE_ENTITY, null);
        }
        try {
            person = personService.saveNewPerson(person);
            logger.info("Person created. {}", person);
            return new ResponseEntity(HttpStatus.CREATED);
        } catch (AlreadyExistsException e) {
            return returnError("Already exists a person with this document. " + e.getMessage(), HttpStatus.UNPROCESSABLE_ENTITY, e);
        } catch (Exception e) {
            return returnError("Unexpected error to create person, contact an administrator", HttpStatus.INTERNAL_SERVER_ERROR, e);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/person", method = RequestMethod.GET)
    public ResponseEntity searchPersons(@RequestParam(value = "name", required = false) String name,
                                        @RequestParam(value = "document", required = false) String document,
                                        @RequestParam(value = "birthdate", required = false) @DateTimeFormat(pattern = "dd/MM/yyyy") Date birthdate,
                                        @RequestParam(value = "sex", required = false) String sex,
                                        @RequestParam(value = "address", required = false) String address,
                                        @RequestParam(value = "page", required = false, defaultValue = "0") Integer page,
                                        @RequestParam(value = "pageSize", required = false, defaultValue = "10") Integer pageSize,
                                        @RequestParam(value = "orderfield", required = false, defaultValue = "name") String orderField,
                                        @RequestParam(value = "order", required = false, defaultValue = "asc") String order) {
        try {
            Page<PersonEntity> persons = personService.searchPersons(name, document, birthdate, SexType.fromString(sex),
                    address, page, pageSize, orderField, order);
            logger.info("Persons finded. {}", persons);
            return new ResponseEntity(persons, HttpStatus.OK);
        } catch (Exception e) {
            return returnError("Unexpected error to search persons, contact an administrator", HttpStatus.INTERNAL_SERVER_ERROR, e);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/person/{id}", method = RequestMethod.GET)
    public ResponseEntity searchPerson(@PathVariable("id") Long personId) {
        try {
            PersonEntity person = personService.searchPerson(personId);
            logger.info("Person finded. {}", person);
            return new ResponseEntity(person, HttpStatus.OK);
        } catch (Exception e) {
            return returnError("Unexpected error to find person by id=" + personId + ", contact an administrator", HttpStatus.INTERNAL_SERVER_ERROR, e);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/person/{id}", method = RequestMethod.POST)
    public ResponseEntity updatePerson(@PathVariable("id") Long targetPersonId,
                                       @RequestBody PersonEntity person) {
        if (person == null || person.getName() == null || person.getDocument() == null ||
                person.getBirthdate() == null || person.getSex() == null) {
            return returnError("Error to update person, required fields: name, document, birthdate and sex", HttpStatus.UNPROCESSABLE_ENTITY, null);
        }
        try {
            person = personService.updatePerson(targetPersonId, person);
            logger.info("Person updated. {}", person);
            return new ResponseEntity(HttpStatus.OK);
        } catch (NotFoundException e) {
            return returnError("Not found person to update with id=" + targetPersonId, HttpStatus.INTERNAL_SERVER_ERROR, e);
        } catch (Exception e) {
            return returnError("Unexpected error to update person, contact an administrator", HttpStatus.INTERNAL_SERVER_ERROR, e);
        }
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(value = "/person/{id}", method = RequestMethod.DELETE)
    public ResponseEntity deletePerson(@PathVariable("id") Long targetPersonId) {
        try {
            personService.deletePerson(targetPersonId);
            logger.info("Person deleted. id={}", targetPersonId);
            return new ResponseEntity(HttpStatus.OK);
        } catch (NotFoundException e) {
            return returnError("Not found person to remove with id=" + targetPersonId, HttpStatus.INTERNAL_SERVER_ERROR, e);
        } catch (Exception e) {
            return returnError("Unexpected error to remove person, contact an administrator", HttpStatus.INTERNAL_SERVER_ERROR, e);
        }
    }

    private ResponseEntity returnError(String errorMsg, HttpStatus status, Exception exception) {
        if (exception == null) {
            logger.error(errorMsg);
        } else {
            logger.error(errorMsg, exception);
        }
        ErrorResponse errorResponse = new ErrorResponse(errorMsg, status.toString());
        return new ResponseEntity(errorResponse, status);
    }

}
