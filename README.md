###Dependencias:
    Jdk 8, maven, npm
    
###Tecnologias utilizadas:
    Springboot, Jpa, Banco de dados em memória(H2), Reactjs, Redux, Webpack, Bootstrap, Google Charts
    
###Rodando projeto:
Na raiz do projeto:

    > mvn clean install
        -- o comando acima baixa as dependênncias e compila tanto o front e back end
            (lembre-se que é necessario ter o npm instalado para baixar e compilar o front end).
        -- para funcionar necessita estar no linux
            (pois para jogar os arquivos compilados do front (resources/public) para o springboot
            subir a aplicação web, o comando rm -rf nao funciona no windows, para rodar no windows
            é necessario compilar e executar semparadamente o front do back end)
    
    > java -jar target/person-manager-0.0.1-SNAPSHOT.jar
            -- o comanda ira subir a aplicação spring boot com a aplicação web junto.

    Agora no navegador basta acessar http://localhost:8080

###Rodando front-end separado com o Webpack(para desenvolvimento):

    A aplicação web se encontra neste caminho ({raiz_do_projeto}/src/main/webapp)

    > npm install
        -- basta rodar uma vez para baixar as dependencias do front-end

    > npm start
        -- ira rodar o Webpack e subira um servidor web em http://localhost:3000

###Rodando apenas o back-end Springboot:

    > mvn clean install -Dexec.skip (compila apenas o back-end verificar se a pasta src/main/ressources/public está vazia ou nao existe)

    > java -jar target/person-manager-0.0.1-SNAPSHOT.jar
                  -- o comanda ira subir a aplicação spring boot sem a aplicação web.


# NEPPO - Teste Front + Back Java    

#Projeto a ser desenvolvido:

Desenvolver projeto web based utilizando as tecnologias HTML5, Javascript, CSS3 e Spring Boot.
Funcionalidades que deverão ser desenvolvidas:

	CRUD de pessoas
		Criar tela de pesquisa, cadastro, edição e exclusão de pessoas.
			Objeto "Pessoa" terá os seguintes atributos: nome, data nascimento, documento de identificação, sexo, endereco.
			Incluir validação de obrigatório para todos os campos: documento de identificação, nome, data nascimento e sexo.
			
	Relatorio de pessoas
		Criar tela que contenha resultados graficos das pessoas.
			Grafico da faixa de idades das pessoas, faixas: ["0 a 9", "10 a 19", "20 a 29", "30 a 39", "Maior que 40"]
			Grafico contendo quantidade de pessoas que tenham sexo masculino e feminino
	
	Criar serviços RESTful para comunicação entre o back e o front

#Pré requisitos, tecnologias:

	Javascript:
		- Criar projeto modularizado, aplicar conceitos ECMAScript 6 modules, AMD ou CommonJS(http://2ality.com/2014/09/es6-modules-final.html)

	HTML e CSS:
		- Utilizar HTML5
		- Utilizar CSS3(https://developer.mozilla.org/pt-BR/docs/Web/CSS/CSS3) & SASS(se preciso)
		- Desenvolver design responsivo

	UX:
		- Desenvolva pensando e aplicando conceitos de UX (http://designculture.com.br/conceitos-fundamentais-de-um-bom-ux)
	
	Java:
		- Criar back-end utilizando Spring Boot
		- Criar camada de persistência utilizando JPA+Hibernate
		
#Hint (dicas):
	Abaixo contem algumas dicas para tornar seu projeto melhor:
		- Tente desenvolver testes unitarios
		- Tente utilizar padroes de projetos(design patterns)
		- Tente desenvolver usando o conceito single Page Application (SPA)
		- Tente usar algum framework JS, como por exemplo: Angular, Vue.js, React, AngularJS.
	  	- Tente utilizar algum framework de view, como por exemplo: bootstrap, google-material-design, metro-io
		- Tente usar Flux/Redux
		- Tente utilizar alguma ferramenta facilitadora para "unificar, minificar e obfuscar arquivos estaticos", como: webpack, gulp, grunt, etc.
		- Tente utilizar algum gerenciador de pacotes javascript(gerenciador de dependencias), como: yarn, NPM, Bower. 
		- Tente ter import de apenas um arquivo JS no arquivo HTML.				
		- Tente ter import de apenas um arquivo CSS no arquivo HTML.

Crie um fork deste repo, faça o desenvolvimento necessário e nos envie a URL.
Caso tenha problemas em criar um fork, siga o link [How to Fork in bitbucket](https://confluence.atlassian.com/bitbucket/forking-a-repository-221449527.html#ForkingaRepository-HowtoForkaRepository)

Sugestões de IDE: [VSCODE](https://code.visualstudio.com/), [WebStorm](https://www.jetbrains.com/webstorm/), [Atom](https://atom.io/), [SublimeText](https://www.sublimetext.com/), [IntelliJ](https://www.jetbrains.com/idea/), [Eclipse](https://www.eclipse.org/), [NetBeans](https://netbeans.org/)
